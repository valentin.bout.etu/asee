#include <linux/module.h>
#define INCLUDE_VERMAGIC
#include <linux/build-salt.h>
#include <linux/elfnote-lto.h>
#include <linux/export-internal.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

#ifdef CONFIG_UNWINDER_ORC
#include <asm/orc_header.h>
ORC_HEADER;
#endif

BUILD_SALT;
BUILD_LTO_INFO;

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__section(".gnu.linkonce.this_module") = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif



static const struct modversion_info ____versions[]
__used __section("__versions") = {
	{ 0x6bc3fbc0, "__unregister_chrdev" },
	{ 0xecc78457, "kobject_put" },
	{ 0x73895700, "__register_chrdev" },
	{ 0x122c3a7e, "_printk" },
	{ 0xeb233a45, "__kmalloc" },
	{ 0xfb890f0b, "class_create" },
	{ 0x41b15bc9, "device_create" },
	{ 0xa6ef1646, "kernel_kobj" },
	{ 0x10bd90f2, "kobject_create_and_add" },
	{ 0xa3efd5d1, "sysfs_create_file_ns" },
	{ 0xbcab6ee6, "sscanf" },
	{ 0x37a0cba, "kfree" },
	{ 0xbdfb6dbb, "__fentry__" },
	{ 0x5b8239ca, "__x86_return_thunk" },
	{ 0xc3aaf0a9, "__put_user_1" },
	{ 0x167e7f9d, "__get_user_1" },
	{ 0x3c3ff9fd, "sprintf" },
	{ 0xab632ed, "module_put" },
	{ 0x47143bb3, "try_module_get" },
	{ 0xf23bcaeb, "device_destroy" },
	{ 0xfdfe3852, "class_destroy" },
	{ 0x76800044, "module_layout" },
};

MODULE_INFO(depends, "");


MODULE_INFO(srcversion, "0407622983A4737B8D0E5C8");
