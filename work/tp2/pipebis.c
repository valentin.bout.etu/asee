/* 
 * pipebis.c: Creates a read-only char device that says how many times 
 * you have read from the dev file 
 */ 
 
#include <linux/atomic.h> 
#include <linux/cdev.h> 
#include <linux/delay.h> 
#include <linux/device.h> 
#include <linux/fs.h> 
#include <linux/init.h> 
#include <linux/kernel.h> /* for sprintf() */ 
#include <linux/module.h> 
#include <linux/printk.h> 
#include <linux/types.h> 
#include <linux/uaccess.h> /* for get_user and put_user */ 
#include <linux/version.h> 
 
#include <asm/errno.h> 
 
/*  Prototypes - this would normally go in a .h file */ 
static int device_open(struct inode *, struct file *); 
static int device_release(struct inode *, struct file *); 
static ssize_t device_read(struct file *, char __user *, size_t, loff_t *); 
static ssize_t device_write(struct file *, const char __user *, size_t, 
                            loff_t *); 
 
#define SUCCESS 0 
#define DEVICE_NAME "asee_mod" /* Dev name as it appears in /proc/devices   */ 
#define BUF_LEN 16 /* Max length of the message from the device */ 
 
/* Global variables are declared as static, so are global within the file. */ 

static char *msg; /* The msg the device will give when asked */ 

static struct kobject *pipebis_module;

static int asee_buf_count = 0;

static int asee_buf_size = BUF_LEN;

static ssize_t asee_buf_size_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sprintf(buf, "%d\n", asee_buf_size);
}

static ssize_t asee_buf_size_store(struct kobject *kobj, struct kobj_attribute *attr, char *buf, size_t count)
{
    int old_size = asee_buf_size;
    sscanf(buf, "%du", &asee_buf_size);
    if (asee_buf_size <= asee_buf_count) {
        asee_buf_size = old_size;
        pr_err("New size for buffer should be less than current content in it");
        return count;
    }

    char * second_message = kcalloc(asee_buf_size, sizeof(char), GFP_KERNEL);
    for(int cpt = 0; cpt < old_size; cpt++) {
        second_message[cpt] = msg[cpt];
    }
    kfree(msg);
    msg = second_message;
    // TODO: incresae buffer size
    return count;
}

static struct kobj_attribute asee_buf_size_attribute = __ATTR(asee_buf_size, 0660, asee_buf_size_show, (void *)asee_buf_size_store);


static ssize_t asee_buf_count_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sprintf(buf, "%d\n", asee_buf_count);
}

static ssize_t asee_buf_count_store(struct kobject *kobj, struct kobj_attribute *attr, char *buf, size_t count)
{
    return count;
}
 
static struct kobj_attribute asee_buf_count_attribute = __ATTR(asee_buf_count, 0660, asee_buf_count_show, (void *)asee_buf_count_store);

static int major; /* major number assigned to our device driver */ 
 
enum { 
    CDEV_NOT_USED = 0, 
    CDEV_EXCLUSIVE_OPEN = 1, 
}; 
 
/* Is device open? Used to prevent multiple access to device */ 
static atomic_t already_open = ATOMIC_INIT(CDEV_NOT_USED); 
 
static struct class *cls; 
 
static struct file_operations pipebis_fops = { 
    .read = device_read, 
    .write = device_write, 
    .open = device_open, 
    .release = device_release, 
}; 
 
static int __init pipebis_init(void) 
{ 
    major = register_chrdev(0, DEVICE_NAME, &pipebis_fops); 
 
    if (major < 0) { 
        pr_alert("Registering char device failed with %d\n", major); 
        return major; 
    } 
 
    pr_info("I was assigned major number %d.\n", major); 

    msg = kcalloc(asee_buf_size, sizeof(char), GFP_KERNEL);
 
#if LINUX_VERSION_CODE >= KERNEL_VERSION(6, 4, 0) 
    cls = class_create(DEVICE_NAME); 
#else 
    cls = class_create(THIS_MODULE, DEVICE_NAME); 
#endif 
    device_create(cls, NULL, MKDEV(major, 0), NULL, DEVICE_NAME); 
 
    pr_info("Device created on /dev/%s\n", DEVICE_NAME); 
    
    pipebis_module = kobject_create_and_add("asee_mod", kernel_kobj);
    if (!pipebis_module) return -ENOMEM;

    int error = 0;
    error = sysfs_create_file(pipebis_module, &asee_buf_size_attribute.attr);
    error += sysfs_create_file(pipebis_module, &asee_buf_count_attribute.attr);
    if (error) {
        pr_info("failed to create a asee variable in /sys/kernel/asee_mod\n");
        return error;
    }

    return SUCCESS; 
} 
 
static void __exit pipebis_exit(void) 
{ 
    device_destroy(cls, MKDEV(major, 0)); 
    class_destroy(cls); 
 
    /* Unregister the device */ 
    unregister_chrdev(major, DEVICE_NAME); 
    kobject_put(pipebis_module);
} 
 
/* Methods */ 

/* Called when a process tries to open the device file, like 
 * "sudo cat /dev/pipebis" 
 */ 
static int device_open(struct inode *inode, struct file *file) 
{  
    if (atomic_cmpxchg(&already_open, CDEV_NOT_USED, CDEV_EXCLUSIVE_OPEN)) 
        return -EBUSY; 

    try_module_get(THIS_MODULE); 
 
    return SUCCESS; 
} 
 
/* Called when a process closes the device file. */ 
static int device_release(struct inode *inode, struct file *file) 
{ 
    /* We're now ready for our next caller */ 
    atomic_set(&already_open, CDEV_NOT_USED); 
 
    /* Decrement the usage count, or else once you opened the file, you will 
     * never get rid of the module. 
     */ 
    module_put(THIS_MODULE); 
 
    return SUCCESS; 
} 
 
/* Called when a process, which already opened the dev file, attempts to 
 * read from it. 
 */ 
static int last_writen_bytes = 0;
static int last_read_bytes = 0;
static ssize_t device_read(struct file *filp, /* see include/linux/fs.h   */ 
                           char __user *buffer, /* buffer to fill with data */ 
                           size_t length, /* length of the buffer     */ 
                           loff_t *offset) 
{ 
    /* Number of bytes actually written to the buffer */ 
    int bytes_read = 0; 
    char *msg_ptr = msg; 
 
    /* Actually put the data into the buffer */ 
    while (asee_buf_count > 0) { 
        if (last_read_bytes >= asee_buf_size) {
            last_read_bytes = 0;
        }
        /* The buffer is in the user data segment, not the kernel 
         * segment so "*" assignment won't work.  We have to use 
         * put_user which copies data from the kernel data segment to 
         * the user data segment. 
         */ 
        
        put_user(*(msg_ptr + last_read_bytes), buffer++);
        // Try to clear the read
        *(msg_ptr + last_read_bytes) = '\0';
        last_read_bytes++;
        asee_buf_count--;
        bytes_read++;
    } 
    *offset = last_read_bytes; 
 
    /* Most read functions return the number of bytes put into the buffer. */ 
    return bytes_read; 
} 
 
/* Called when a process writes to dev file: echo "hi" > /dev/hello */ 
static ssize_t device_write(struct file *filp, const char __user *buff, 
                            size_t len, loff_t *off) 
{ 
    int bytes_writen = 0;

    int current_bytes = last_writen_bytes;

    for (int cpt = 0; cpt < len; cpt++) {
        if (current_bytes >= asee_buf_size) {
            current_bytes = 0;
        }
        get_user(*(msg + current_bytes), buff + cpt);
        current_bytes++;
        bytes_writen++;
        if (asee_buf_count < asee_buf_size) {
            asee_buf_count++;
        }
    }
    last_read_bytes = (last_read_bytes + bytes_writen) % asee_buf_count;

    last_writen_bytes = current_bytes;
    *off = last_writen_bytes; 

    return bytes_writen; 
} 
 
module_init(pipebis_init); 
module_exit(pipebis_exit); 
 
MODULE_LICENSE("GPL");
